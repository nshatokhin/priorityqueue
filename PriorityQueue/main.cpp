#include "PriorityQueue.h"

#include <iostream>
#include <conio.h>

int main()
{
	PriorityQueue<uint32_t> pq(6, 0, 9999999);

	uint32_t arr[5] = {6, 3, 9, 5, 1};
	
	size_t * indices = pq.buildHeap(arr, 5);

	std::cout << "Indices: ";
	for (uint32_t i = 0; i < pq.heapSize(); i++)
	{
		std::cout << indices[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "Objects: ";
	for (uint32_t i = 0; i < pq.heapSize(); i++)
	{
		std::cout << pq.objects()[i] << " ";
	}
	std::cout << std::endl;

	std::cout << "Removing..." << std::endl;

	pq.remove(2);

	std::cout << "Indices: ";
	for (uint32_t i = 0; i < pq.heapSize(); i++)
	{
		std::cout << pq.indices()[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "Objects: ";
	for (uint32_t i = 0; i < pq.heapSize(); i++)
	{
		std::cout << pq.objects()[i] << " ";
	}
	std::cout << std::endl;

	std::cout << "Updating..." << std::endl;
	
	pq.update(3, 2);

	std::cout << "Indices: ";
	for (uint32_t i = 0; i < pq.heapSize(); i++)
	{
		std::cout << pq.indices()[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "Objects: ";
	for (uint32_t i = 0; i < pq.heapSize(); i++)
	{
		std::cout << pq.objects()[i] << " ";
	}
	std::cout << std::endl;

	std::cout << "Inserted: " << pq.insert(13) << std::endl;

	std::cout << "Indices: ";
	for (uint32_t i = 0; i < pq.heapSize(); i++)
	{
		std::cout << pq.indices()[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "Objects: ";
	for (uint32_t i = 0; i < pq.heapSize(); i++)
	{
		std::cout << pq.objects()[i] << " ";
	}
	std::cout << std::endl;

	std::cout << "Inserted: " << pq.insert(4) << std::endl;

	std::cout << "Indices: ";
	for (uint32_t i = 0; i < pq.heapSize(); i++)
	{
		std::cout << pq.indices()[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "Objects: ";
	for (uint32_t i = 0; i < pq.heapSize(); i++)
	{
		std::cout << pq.objects()[i] << " ";
	}
	std::cout << std::endl;

	std::cout << "Min: " << pq.min() << std::endl;

	std::cout << "Popping: ";
	while (pq.heapSize() > 0)
	{
		std::cout << pq.extractMin() << " ";
	}
	std::cout << std::endl;

	std::cout << "Min in empty: " << pq.extractMin() << std::endl;
	_getch();

	return 0;
}